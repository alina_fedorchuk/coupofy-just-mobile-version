define(function () {
    "use strict";

    var optionDefaults = {

    };

    function Request(route, options, rest) {
        options = options || {};
        this.route = route;
        this.url = rest.getUrl(route);
        this.cid = _.uniqueId('rest.request');
        this.sentData = null;
    }

    _.extend(Request.prototype, Backbone.Events, {

        send: function (data) {
            var that = this;
            this.sentData = data;

            var abortTimeout;

            function ajaxBeforeSend(xhr, settings) {
                var context = settings.context
                if (settings.beforeSend.call(context, xhr, settings) === false ||
                    triggerGlobal(settings, context, 'ajaxBeforeSend', [xhr, settings]) === false)
                    return false

                triggerGlobal(settings, context, 'ajaxSend', [xhr, settings])
            }

            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4){
                    clearTimeout(abortTimeout);
                    if(xhr.status == 200) {
                        var data = JSON.parse(xhr.responseText);
                        that.trigger(data.e,data.d);
                    } else {
                        that.trigger('error', 'SERVER_ERROR');
                    }
                }
            };
            xhr.open("POST", this.url + '?_=' + Date.now(), true);
            xhr.setRequestHeader('accept','application/json');
            xhr.setRequestHeader('content-type','application/x-www-form-urlencoded');
            xhr.send(serializeData(data));

            abortTimeout = setTimeout(function() {
                xhr.abort();
                that.trigger('error', 'SERVER_ERROR_TIMEOUT');
            },5000); //todo: change this timeout to options.timeout (dynamically assigned + with defaults)

        }

    });

    function serializeData(data) {
        var result = [];
        Object.keys(data).forEach(function(id){
            result.push(encodeURIComponent(id) + '=' + encodeURIComponent(data[id]));
        })
        return result.join('&');
    }

    return Request;

});