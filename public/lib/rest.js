define(function (require) {
    "use strict";

    var Request = require('lib/rest/Request');

    function Rest() {
        var that = this;
        this.domain = 'pbn.host';
        this.port = '80';
    }

    Rest.prototype = {

        req: function (route, options) {
            return new Request(route, options, this);
        },

        getUrl: function (route) {
            route = route || "";
            return 'http://' + this.domain + ':' + this.port + '/' + route;
        }

    };

    return new Rest();

});