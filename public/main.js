;
(function () {
  if (!window.Website) {
    window.Website = {
      API: null
    }
  }

  var domReady = false
  $(document).ready(function () {
    domReady = true
  })

  window.Website.API = {
    call: function (route, data, callback) {
      $.ajax({
        type: 'POST',
        url: Website.CFG.apiUrl + route,
        data: data,
        success: function (res) {
          callback(null, res)
        },
        error: function (err) {
          if (err && err.responseJSON) {
            callback(err.responseJSON.d)
          } else {
            callback(err)
          }
        },
        dataType: 'json'
      })
    }
  }
})()

/**
 * Bind deal popup to all deals displayed
 */
$('body').on('click', '.dealDisplayItem .redeem-link', function (e) {
  e.preventDefault()
  var $el = $(e.target).closest('.redeem-link')

  window.open($el.attr('href'))

  setTimeout(function () {
    if ($el.attr('data-wloc')) {
      window.location = $el.attr('data-wloc')
    }
  }, 1)
});

$(document).ready(function () {
  var $body = $('body'),
    $window = $(window);

  $('.coupon .redeem-area .b-social').each(function (i, e) {
    var shareTitle = $(e).attr('data-share-title');
    var shareURL = window.location.href;

    $(e).find('.rrssb-buttons').rrssb({
      title: shareTitle,
      url: shareURL,
      description: shareTitle
    });

  });

  $('.blog .postItem .info .rrssb-buttons').each(function (i, e) {
    var shareTitle = $(e).attr('data-share-title');
    var shareURL = $(e).attr('data-share-url');

    $(e).rrssb({
      title: shareTitle,
      url: shareURL,
      description: shareTitle
    });

  });

  // Blog Post
  if ($('.blog .postItem.single').length > 0) {
    $('.blog .postItem.single .st_facebook_large').each(function (i, e) {
      var $existing = $(e);
      var shareURL = $(e).attr('st_url');
      var shareTitle = $(e).attr('st_title');
      var shareImage = $(e).attr('st_image');
      var $holder = $(e).parent();
      $holder.html('<ul class="rrssb-buttons clearfix"><li class="rrssb-facebook">   <a target="_blank" href="" title="Facebook" class="popup">  <span class="rrssb-icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 29 29"><path d="M26.4 0H2.6C1.714 0 0 1.715 0 2.6v23.8c0 .884 1.715 2.6 2.6 2.6h12.393V17.988h-3.996v-3.98h3.997v-3.062c0-3.746 2.835-5.97 6.177-5.97 1.6 0 2.444.173 2.845.226v3.792H21.18c-1.817 0-2.156.9-2.156 2.168v2.847h5.045l-.66 3.978h-4.386V29H26.4c.884 0 2.6-1.716 2.6-2.6V2.6c0-.885-1.716-2.6-2.6-2.6z"/></svg></span>  <span class="rrssb-text">facebook</span></a> </li> <li class="rrssb-linkedin" data-initwidth="14.285714285714286" data-size="58" style="width: 14.2857%;"><a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://kurtnoble.com/labs/rrssb/index.html&amp;title=Ridiculously%20Responsive%20Social%20Sharing%20Buttons&amp;summary=Responsive%20social%20icons%20by%20KNI%20Labs" class="popup"><span class="rrssb-icon">              <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 28 28">                <path d="M25.424 15.887v8.447h-4.896v-7.882c0-1.98-.71-3.33-2.48-3.33-1.354 0-2.158.91-2.514 1.802-.13.315-.162.753-.162 1.194v8.216h-4.9s.067-13.35 0-14.73h4.9v2.087c-.01.017-.023.033-.033.05h.032v-.05c.65-1.002 1.812-2.435 4.414-2.435 3.222 0 5.638 2.106 5.638 6.632zM5.348 2.5c-1.676 0-2.772 1.093-2.772 2.54 0 1.42 1.066 2.538 2.717 2.546h.032c1.71 0 2.77-1.132 2.77-2.546C8.056 3.593 7.02 2.5 5.344 2.5h.005zm-2.48 21.834h4.896V9.604H2.867v14.73z"></path>              </svg></span><span class="rrssb-text">linkedin</span></a></li><li class="rrssb-twitter"><a target="_blank" href="" title="Twitter" class="popup">  <span class="rrssb-icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 28 28"><path d="M24.253 8.756C24.69 17.08 18.297 24.182 9.97 24.62a15.093 15.093 0 0 1-8.86-2.32c2.702.18 5.375-.648 7.507-2.32a5.417 5.417 0 0 1-4.49-3.64c.802.13 1.62.077 2.4-.154a5.416 5.416 0 0 1-4.412-5.11 5.43 5.43 0 0 0 2.168.387A5.416 5.416 0 0 1 2.89 4.498a15.09 15.09 0 0 0 10.913 5.573 5.185 5.185 0 0 1 3.434-6.48 5.18 5.18 0 0 1 5.546 1.682 9.076 9.076 0 0 0 3.33-1.317 5.038 5.038 0 0 1-2.4 2.942 9.068 9.068 0 0 0 3.02-.85 5.05 5.05 0 0 1-2.48 2.71z"/></svg></span>  <span class="rrssb-text">twitter</span></a> </li> <li class="rrssb-pinterest"><a target="_blank" href="" title="Pinterest" class="popup">  <span class="rrssb-icon"><svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 28 28" enable-background="new 0 0 28 28" xml:space="preserve"><path d="M14.021,1.57C6.96,1.57,1.236,7.293,1.236,14.355c0,7.062,5.724,12.785,12.785,12.785c7.061,0,12.785-5.725,12.785-12.785 C26.807,7.294,21.082,1.57,14.021,1.57z M15.261,18.655c-1.161-0.09-1.649-0.666-2.559-1.219c-0.501,2.626-1.113,5.145-2.925,6.458 c-0.559-3.971,0.822-6.951,1.462-10.116c-1.093-1.84,0.132-5.545,2.438-4.632c2.837,1.123-2.458,6.842,1.099,7.557 c3.711,0.744,5.227-6.439,2.925-8.775c-3.325-3.374-9.678-0.077-8.897,4.754c0.19,1.178,1.408,1.538,0.489,3.168 C7.165,15.378,6.53,13.7,6.611,11.462c0.131-3.662,3.291-6.227,6.46-6.582c4.007-0.448,7.771,1.474,8.29,5.239 c0.579,4.255-1.816,8.865-6.102,8.533L15.261,18.655z"/></svg></span>  <span class="rrssb-text">pinterest</span></a> </li> <li class="rrssb-googleplus" data-initwidth="14.285714285714286" data-size="61" style="width: 14.2857%;">          <!-- Replace href with your meta and URL information.--><a target="_blank" href="https://plus.google.com/share?url=Check%20out%20how%20ridiculously%20responsive%20these%20social%20buttons%20are%20http://kurtnoble.com/labs/rrssb/index.html" class="popup"><span class="rrssb-icon">              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">                <path d="M21 8.29h-1.95v2.6h-2.6v1.82h2.6v2.6H21v-2.6h2.6v-1.885H21V8.29zM7.614 10.306v2.925h3.9c-.26 1.69-1.755 2.925-3.9 2.925-2.34 0-4.29-2.016-4.29-4.354s1.885-4.353 4.29-4.353c1.104 0 2.014.326 2.794 1.105l2.08-2.08c-1.3-1.17-2.924-1.883-4.874-1.883C3.65 4.586.4 7.835.4 11.8s3.25 7.212 7.214 7.212c4.224 0 6.953-2.988 6.953-7.082 0-.52-.065-1.104-.13-1.624H7.614z"></path>              </svg></span><span class="rrssb-text">google+</span></a>        </li><li class="rrssb-email"><a  href="" title="Email">  <span class="rrssb-icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 28 28" enable-background="new 0 0 28 28" xml:space="preserve"><g><path d="M20.111 26.147c-2.336 1.051-4.361 1.401-7.125 1.401c-6.462 0-12.146-4.633-12.146-12.265 c0-7.94 5.762-14.833 14.561-14.833c6.853 0 11.8 4.7 11.8 11.252c0 5.684-3.194 9.265-7.399 9.3 c-1.829 0-3.153-0.934-3.347-2.997h-0.077c-1.208 1.986-2.96 2.997-5.023 2.997c-2.532 0-4.361-1.868-4.361-5.062 c0-4.749 3.504-9.071 9.111-9.071c1.713 0 3.7 0.4 4.6 0.973l-1.169 7.203c-0.388 2.298-0.116 3.3 1 3.4 c1.673 0 3.773-2.102 3.773-6.58c0-5.061-3.27-8.994-9.303-8.994c-5.957 0-11.175 4.673-11.175 12.1 c0 6.5 4.2 10.2 10 10.201c1.986 0 4.089-0.43 5.646-1.245L20.111 26.147z M16.646 10.1 c-0.311-0.078-0.701-0.155-1.207-0.155c-2.571 0-4.595 2.53-4.595 5.529c0 1.5 0.7 2.4 1.9 2.4 c1.441 0 2.959-1.828 3.311-4.087L16.646 10.068z"/></g></svg></span>  <span class="rrssb-text">email</span></a> </li>  </ul>');
      $holder.find('.rrssb-buttons').rrssb({
        title: shareTitle,
        url: shareURL,
        description: shareTitle
      });

    });
  }

  $('.top > a').click(function (e) {
    $('html, body').animate({
      scrollTop: 0
    }, 600)
    e.preventDefault()
    return false
  });

  $('.top > a').each(function (i, e) {
    var $e = $(e),
      $boxHolder = $e.closest('.box-holder')

    if ($boxHolder.length > 0) {
      if ($boxHolder.find('.show-more-holder').length === 0) {
        $e.parent().addClass('noPaging')
      }
    }
  })

  $('.about-store-box > .nav-bar > li.dropdown > a').on('click', function (e) {
    e.preventDefault()
    return false
  })

  $('.read-more-link').click(function (e) {
    var $link = $(this),
      $content = $link.parent().find('.read-more-content')

    $content.css('height', 'auto')
    $link.hide()

    e.preventDefault()
    return false
  })

  function resizeHeaderScroll() {
    var $header = $('#header'),
      $logo = $('#header #logo'),
      $searchBox = $('#header .search-box'),
      $nav = $('#header .nav'),
      scrollY = $(document).scrollTop()

    if (scrollY === 0) {
      $header.removeClass('scrollBelow')
    } else {
      $header.addClass('scrollBelow')
    }

    // Store deal section nav
    if (scrollY > 200) {
      $('#store-deal-section-nav').parent().show()
      $('#store-deal-section-nav').addClass('scrollBelow')
    } else {
      $('#store-deal-section-nav').parent().hide()
      $('#store-deal-section-nav').removeClass('scrollBelow')
    }

    if ($('#store-deal-section-nav').length > 0) {
      var lastActive,
        currentActive
      $('.box-holder .deal-section').each(function (i, e) {
        var $section = $(this),
          sectionName = $section.text(),
          vertical = $section.offset().top

        if ((vertical - 120) < scrollY) {
          currentActive = sectionName
        }
      })

      if (currentActive !== lastActive) {
        $('#store-deal-section-nav .section-link').removeClass('active')
        $('#store-deal-section-nav .section-link').each(function (i, e) {
          if ($(this).text().indexOf(currentActive) === 0) {
            $(this).addClass('active')
            lastActive = currentActive
          }
        })
      }
    }
  }

  resizeHeaderScroll()
  $(document).scroll(resizeHeaderScroll)

  function resizePopup() {
    var wHeight = $window.height(),
      wWidth = $window.width(),
      $bg = $('#popupBg'),
      $box = $('#popupBox')

    // Adjust height
    $bg.height(wHeight).width(wWidth)
  }

  function showPopup($sourceElement, callback) {
    var $bg = $('#popupBg'),
      $box = $('#popupBox')

    $bg.show()

    $box.css({
      width: $sourceElement.width() - 40,
      height: $sourceElement.height() - 40,
      top: $sourceElement.offset().top - $(document).scrollTop(),
      left: $sourceElement.offset().left
    }).show()

    callback()

    setTimeout(function () {
      $box.addClass('visibleStep')
      $bg.addClass('visible')
    }, 5)

    setTimeout(function () {
      $box.addClass('visible')
    }, 600)
  }

  function hidePopup() {
    var $bg = $('#popupBg'),
      $box = $('#popupBox')

    $box.removeClass('visible').removeClass('visibleStep').removeClass('visibleStep2')
    $bg.removeClass('visible')
    setTimeout(function () {
      $box.hide()
      $bg.hide()
      $box.find('.content').html('')
    }, 350)
  }

  function loadPopupContent(url, content) {
    if (url) {
      $.get(url, function (html) {
        $('#popupBox .content').html(html)
      })
    }
  }

  $('#popupBg').click(function () {
    hidePopup();
  })
  $('#popupBox .closeBtn').click(function (e) {
    hidePopup();
    e.preventDefault();
    return false;
  })

  resizePopup()
  $window.on('resize', resizePopup)

  $('body').on('click', '.dealDisplayItem .deal-widget-item', function (e) {
    e.preventDefault()

    var $el = $(e.target).closest('.dealDisplayItem'),
      id = $el.attr('data-id')

    showPopup($el, function () {
      loadPopupContent('/deal/ajax-view?id=' + id)
    })
  })

  function resizeBgImage() {
    var $img = $('#featured-top .bg img'),
      naturalHeight = $img.prop('naturalHeight') || 400,
      naturalWidth = $img.prop('naturalWidth') || 1920
    if ($img.length > 0) {
      if (($('#featured-top').outerHeight() / $('#featured-top').width()) > naturalHeight / naturalWidth) {
        $img.css({
          width: 'auto',
          height: $('#featured-top').outerHeight()
        })
      } else {
        $img.css({
          width: '100%',
          height: 'auto'
        })
      }
    }
  }
  resizeBgImage()
  $window.on('resize', resizeBgImage)

  $('#new-store-comment').on('submit', function (e) {
    var $form = $(this),
      data = {
        path: $form.find('input[name=path]').val(),
        author_name: $form.find('input[name=author_name]').val(),
        author_email: $form.find('input[name=author_email]').val(),
        comment: $form.find('textarea[name=comment]').val()
      }

    $form.parent().append("<p class='sending-msg'>Sending...</p>")
    Website.API.call('Comment/post', data, function (err) {
      $form.find('.field, .submit-field').hide()
      if (err) {
        $form.find('.msg-error').show()
      } else {
        $form.find('.msg-success').show()
      }
      $form.parent().find('.sending-msg').hide()
    })

    e.preventDefault()
    return false
  })

  $('body').on('click', '#popupBox .refresh-popup', function (e) {
    // refresh ajax content in popup
    openHashedDeal();
    e.preventDefault();
  });

  if ($('#sub-go-back').length > 0) {
    $('#sub-go-back').attr('href', $('#sub-go-back').attr('href') + window.location.hash);
  }

  $('body').on('submit', '.storeAlertSubscribeForm', function (e) {
    var $form = $(this),
      data = {
        store_id: $form.find('input[name=store_id]').val(),
        return_to: $form.find('input[name=return_to]').val(),
        email: $form.find('input[name=email]').val()
      }

    $form.html('<p>Sending...</p>')
    Website.API.call('Subscriber/store-subscribe', data, function (err) {
      if (err) {
        $form.html("<strong class='text-error'>You are already subscribed</strong>")
      } else {
        if ($form.parents('#popupBox').length > 0) {
          var expires = new Date();
          expires.setTime(expires.getTime() + (1 * 24 * 60 * 60 * 1000));
          document.cookie = 'deal_email_collected=1;expires=' + expires.toUTCString();
          //todo: fix the subscribe form, add return path to the deal
          //todo: if confirm is required, tell in this message that they should confirm the email and then come back (but already set the cookie)
          //todo: if confirm is not required, just show msg below
          if ($form.attr('data-confirm-req')) {
            $form.html("<strong class='text-success'>Success! You will be alerted when we receive new deals for this store.</strong><br><strong>Please check your email inbox/spam folder for confirmation email. After you confirm your email, you will see the coupon code.</strong>")
          } else {
            $form.html("<strong class='text-success'>Success! You will be alerted when we receive new deals for this store.</strong><br><a href='#show-coupon-code' class='refresh-popup'>Show me the coupon code</a>")
          }
        } else {
          $form.html("<strong class='text-success'>Success! You will be alerted when we receive new deals for this store.</strong>")
        }
      }
    })

    e.preventDefault()
    return false
  })

  $('#footer-subscribe-form').on('submit', function (e) {
    var $form = $('#footer-subscribe-form'),
      data = {
        email: $('#footer-subscribe-form input[name=email]').val(),
        options: []
      }

    if ($form.find('input[name=chkbox_weekly_best_deals]').is(':checked')) {
      data.options.push('best-deals-weekly')
    }

    if ($form.find('input[name=chkbox_weekly_new_stores]').is(':checked')) {
      data.options.push('new-stores-weekly')
    }

    if ($form.find('input[name=chkbox_website_news]').is(':checked')) {
      data.options.push('website-news-monthly')
    }

    if ($form.find('input[name=chkbox_blog]').is(':checked')) {
      data.options.push('blog')
    }

    Website.API.call('Subscriber/subscribe', data, function (err) {
      if (err) {
        alert('You are already subscribed')
      } else {
        alert('Thank you for subscribing!')
      }
    })

    e.preventDefault()
  })

  /**
   * Search
   */

  function handleStoreSearch(query) {
    query = query.split('Store:').pop().trim()
    $.getJSON('/SearchTag/suggest', {
      q: query
    }, function (data) {
      if (!data || data.e !== 'result') return
      data = data.d
      if (data.stores && data.stores.length > 0) {
        data.stores.forEach(function (store) {
          if (store.name.trim() === query.trim()) {
            window.location = '/' + store.slug
          }
        })
      }
    })
  }

  function handleDealTypeSearch(query) {
    query = query.split('Deal Type:').pop().trim()
    $.getJSON('/SearchTag/suggest', {
      q: query
    }, function (data) {
      if (!data || data.e !== 'result') return
      data = data.d
      if (data.types && data.types.length > 0) {
        data.types.forEach(function (type) {
          if (type.name === query) {
            window.location = '/type/' + type.slug
          }
        })
      }
    })
  }

  $('.header-bar .search-box .newtag').autoComplete({
    source: function (input, callback) {
      $.getJSON('/SearchTag/suggest', {
        q: input
      }, function (data) {
        if (!data || data.e !== 'result') return callback([])
        data = data.d
        var suggestions = []
        if (data.stores && data.stores.length > 0) {
          data.stores.forEach(function (store) {
            suggestions.push({
              label: "<img src='/m/" + store.logo_s.u + "' class='store-logo' /> " + store.name,
              value: 'Store: ' + store.name
            })
          })
        }
        if (data.tags && data.tags.length > 0) {
          data.tags.forEach(function (tag) {
            suggestions.push(tag.t)
          })
        }
        if (data.types && data.types.length > 0) {
          data.types.forEach(function (type) {
            suggestions.push('Deal Type: ' + type.name)
          })
        }
        callback(suggestions)
      })
    },
    renderItem: function (item, search) {
      search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&')
      var re = new RegExp('(' + search.split(' ').join('|') + ')', 'gi')
      if (typeof item === 'object') {
        return '<div class="autocomplete-suggestion" data-val="' + item.value + '">' + item.label.replace(re, '<strong>$1</strong>') + '</div>'
      } else {
        return '<div class="autocomplete-suggestion" data-val="' + item + '">' + item.replace(re, '<strong>$1</strong>') + '</div>'
      }
    },
    minChars: 3,
    delay: 150,
    cache: true,
    onSelect: function (event, query, item) {
      if (query.indexOf('store-logo') >= 0) {
        // query = "Store: " + query.split(">").pop().trim()
        $('.header-bar .search-box .newtag').val(query)
        handleStoreSearch(query)
      } else if (query.indexOf('Deal Type:') >= 0) {
        handleDealTypeSearch(query)
      } else {
        $('#searchform').submit()
      }
    }
  })
  $('#searchform').on('submit', function (e) {
    var query = $('.header-bar .search-box .newtag').val()
    if (query.indexOf('Store:') >= 0) {
      handleStoreSearch(query)
      e.preventDefault()
      return false
    } else if (query.indexOf('Deal Type:') >= 0) {
      handleDealTypeSearch(query)
      e.preventDefault()
      return false
    }
    return true
  })

  /**
   * Search page
   */
  if ($('#search-filter-form').length > 0) {
    (function () {
      var $form = $('#search-filter-form'),
        $qInput = $form.find('input[name="q"]'),
        $categoryInput = $form.find('select[name="category[]"]'),
        $typeInput = $form.find('select[name="type[]"]'),
        $storeInput = $form.find('select[name="store[]"]'),
        $paymentInput = $form.find('select[name="payment_method[]"]'),
        $staffPicksInput = $form.find('select[name="staff_pick"]')

      function onValChange(e) {
        $('#content .search-results').addClass('loading')

        var data = {}
        $form.serializeArray().forEach(function (field) {
          data[field.name] = field.value
        })

        $.post('/search', data, function (data) {
          $('#content .search-results .box-holder').html(data)
          stButtons.locateElements()
          $('#content .search-results').removeClass('loading')
        })

        $('#searchform .newtag').val($qInput.val())

        if (e && e.preventDefault) {
          e.preventDefault()
        }
      }

      $form.on('submit', onValChange)

      $staffPicksInput.on('change', onValChange)

      $categoryInput.ajaxChosen({
        dataType: 'json',
        type: 'POST',
        url: '/Category/selectInputFind'
      }, {
        loadingImg: '../vendor/loading.gif'
      }, {
        placeholder_text_multiple: 'Select categories'
      }).on('change', onValChange)

      $typeInput.ajaxChosen({
        dataType: 'json',
        type: 'POST',
        url: '/DealType/selectInputFind'
      }, {
        loadingImg: '../vendor/loading.gif'
      }, {
        placeholder_text_multiple: 'Select deal types'
      }).on('change', onValChange)

      $storeInput.ajaxChosen({
        dataType: 'json',
        type: 'POST',
        url: '/Store/selectInputFind'
      }, {
        loadingImg: '../vendor/loading.gif'
      }, {
        placeholder_text_multiple: 'Select stores'
      }).on('change', onValChange)

      $paymentInput.chosen().on('change', onValChange)
    })()
  }

  /**
   * Quick search on store pages
   */
  var dealQuickSearchActive = false

  function dealQuickSearchValChange(e) {
    if (dealQuickSearchActive) return

    var $form = $('.deal-quick-search'),
      $dealList = $form.parent().find('.deals-list')

    dealQuickSearchActive = true

    $dealList.addClass('loading')
    $.get('/quick-search?' + $form.serialize(), function (data) {
      $dealList.replaceWith(data)
      stButtons.locateElements()
      setTimeout(function () {
        dealQuickSearchActive = false
      }, 500)
    })
  }
  if ($('.deal-quick-search').length > 0) {
    $('.deal-quick-search').on('submit', function (e) {
      dealQuickSearchValChange()
      e.preventDefault()
      return false
    })
    $('.deal-quick-search .query-input input').on('keydown change', dealQuickSearchValChange)
    $('.deal-quick-search .toggles input').change(dealQuickSearchValChange)
  }

  /**
   * Store Score Submit
   */
  function setReviewScoreDisplay(score) {
    $('.about-store-box .review-score .icon').each(function (i, e) {
      if (i < score) {
        $(e).removeClass('icon-star-o').addClass('icon-star')
      } else {
        $(e).removeClass('icon-star').addClass('icon-star-o')
      }
    })
  }

  $('.about-store-box .review-score .icon').click(function () {
    var $star = $(this),
      score = $star.index() + 1

    if ($star.parent().find('.submitted-msg').length > 0) {
      return
    }

    $star.parent().append("<span class='submitted-msg success'>Thank you! Score submitted</span>")
    var $countN = $star.parent().parent().find('.num-reviews span[itemprop=ratingCount]')
    $countN.text(($countN.text() * 1) + 1)

    Website.API.call('Store/submitScore', {
      store_id: $('#main').attr('data-store-id'),
      score: score
    }, function (err) {})
  })
  $('.about-store-box .review-score .icon').hover(function () {
    var $star = $(this),
      score = $star.index() + 1

    if ($star.parent().find('.submitted-msg').length > 0) {
      return
    }

    setReviewScoreDisplay(score)
  })
  $('.about-store-box .review-score').mouseleave(function () {
    if ($(this).find('.submitted-msg').length > 0) {
      return
    }
    setReviewScoreDisplay($(this).find('span[itemprop=ratingValue]').text() * 1)
  })

  $('.togglelink').click(function (e) {
    $($(this).attr('data-target')).slideToggle(300)
    e.preventDefault()
  })

  function openHashedDeal() {
    var id = window.location.hash.split('#d-').pop(),
      $el = $('.dealDisplayItem[data-id=' + id + ']')

    if (!$el) return

    showPopup($el, function () {
      loadPopupContent('/deal/ajax-view?id=' + id)
    })
  }
  if (window.location.hash && window.location.hash.indexOf('#d-') === 0) {
    openHashedDeal();
  }
})