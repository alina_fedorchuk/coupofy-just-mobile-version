var gulp = require('gulp');
var concat = require('gulp-concat');
var path = require('path');
var less = require('gulp-less');
var webserver = require('gulp-webserver');
var path = require('path');

gulp.task("optimize-js", function () {
  return gulp.src([
      'public/lib/jquery.min.js',
      'public/lib/chosen.js',
      'public/lib/jquery.auto-complete.min.js',
      'public/lib/rrssb.js',
      'public/main.js',
      'public/bootstrap/js/bootstrap,min.js'
    ])
    .pipe(concat('bundle.js'))
    .pipe(gulp.dest('./public/'));
});

gulp.task('less', function () {
  return gulp.src(['less/*.less','public/bootstrap/css/bootstrap.css'])
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(concat('style.css'))
    .pipe(gulp.dest('./public/styles'));
});



gulp.task('webserver', function() {
  gulp.src('./')
    .pipe(webserver({
      livereload: true,
      directoryListing: true,
      open: true
    }));
});

gulp.task('build', ['optimize-js']);

gulp.task('default', ['webserver','less']);
